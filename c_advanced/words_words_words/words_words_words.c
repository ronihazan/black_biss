// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
// ------------------------------ functions -----------------------------

int letters_3d_num(int num, int power, int* args_num){
	int result = 0;
	int digit1[] = { 0, 3, 3, 5, 4, 4, 3, 5, 5, 4 };
	int digit2[] = { 0, 0, 6, 6, 5, 5, 5, 7, 6, 6};
	int teens[] = { 3, 6, 6, 8, 8, 7, 7, 9, 8, 8 };
	int powers[] = { 0, 8, 7};
	int level = 0;
	if (num / 100 != 0) {
		++* args_num;
		result += digit1[num/100] + 7;
	}
	num %= 100;
	if (num / 10 != 1) {
		result += digit2[num / 10] + digit1[num % 10];
	}
	else {
		result += teens[num % 10];
	}
	if (num != 0) { ++* args_num; }
	return result + powers[power];

}
/*Returns how many letters are in the num recieved. (including "and")
the number has to be between 0 and 999,999,999*/
int letters_num(int num) {
	int result = 0;
	int power = 0;
	int args_in_num = 0;
	while (num != 0) {
		result += letters_3d_num(num % (int)pow(10, 3), power, &args_in_num);
		num /= (int)pow(10, 3);
		power += 1;
	}
	if (args_in_num > 1) { result += 3; }
	return result;
}


//Returns how many letters are in the numbers between to range given.
int count_letters_range(int from, int to) {
	int result = 0;
	for (int i = from; i <= to; ++i) {
		result += letters_num(i);
	}
	return result;
}


int main(int argc, char* argv[])
{
	printf("The numbers from 1 to 100 contain %d letters.\n", count_letters_range(1, 100));
}
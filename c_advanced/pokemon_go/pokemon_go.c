// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ structs -----------------------------
typedef enum effectiveness {FIRE, WATER, AIR, EARTH} Effectiveness;


typedef struct pokemon {
	char* name;
	double attack;
	double defense;
	Effectiveness effectiveness;
} Pokemon;

// ------------------------------ functions -----------------------------
int first_effectiveness(Effectiveness effect1, Effectiveness effect2) {

	int table_of_effects[4][4] = {
		{2, 1, 3, 2},
		{3, 2, 2, 1},
		{1, 2, 2, 3},
		{2, 3, 1, 2}
	};

	return table_of_effects[effect1][effect2];
}


void winner(Pokemon pokemon1, Pokemon pokemon2) {
	int effect_total = 4;
	int first_effect = first_effectiveness(pokemon1.effectiveness, pokemon2.effectiveness);
	double first_strength = first_effect * pokemon1.attack / pokemon2.defense;
	double second_strength = ((double)effect_total - (double)first_effect) * pokemon2.attack / pokemon1.defense;
	if (first_strength > second_strength) {
		printf("%s is the stronger Pokemon!\n", pokemon1.name);
	}
	else if (first_strength < second_strength) {
		printf("%s is the stronger Pokemon!\n", pokemon2.name);
	}
	else {
		printf("Both pokemons are in equal strength!\n");
	}
}

int main(int argc, char* argv[]) {
	Pokemon	charizard = { "Charizard", 50.0, 40.0, FIRE};
	Pokemon squirtle = { "Squirtle", 50.0, 40.0, WATER };
	winner(charizard, squirtle);
	return 0;
}
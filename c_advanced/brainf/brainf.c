#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#define MEMORY_SIZE (1000)

//program takes file from argv[1], runs it as BF code.
int main(int argc, char** argv) {
    int code_pointer = 0, memory_pointer;
    int memory[MEMORY_SIZE];
    int code_size = 1000;
    char* code = (char*)malloc(code_size);
    int commands_amount = 0, loop_counter = 0;
    FILE* stream = fopen(argv[1], "r");

    //To reduce run time, we will copy the code to an array.
    for (char c = fgetc(stream); c != EOF; c = fgetc(stream)){
        if (c == '+' || c == '-' || c == '[' || c == ']' ||
            c == '.' || c == ',' || c == '<' || c == '>') {
            code[code_pointer] = c;
            ++code_pointer;
            ++commands_amount;
        }
        if (code_pointer >= code_size) {
            code_size += 250;
            code = (char*)realloc(code, code_size);
        }
    }

    code_pointer = 0;
    fclose(stream);

    //reset the memory.
    for (memory_pointer = 0; memory_pointer < MEMORY_SIZE; memory_pointer++) {
        memory[memory_pointer] = 0;
    }
    memory_pointer = 0;

    //run the code.
    for (code_pointer = 0; code_pointer < commands_amount; code_pointer++) {
        //add one to memory.
        if (code[code_pointer] == '+') {
            memory[memory_pointer]++;
        }
        //reduce one from memory.
        else if (code[code_pointer] == '-') {
            memory[memory_pointer]--;
        }
        //add one to memory pointer.
        else if (code[code_pointer] == '>') {
            memory_pointer++;
        }
        //reduce one from memory pointer.
        else if (code[code_pointer] == '<') {
            memory_pointer--;
        }
        //print char.
        else if (code[code_pointer] == '.') {
            putchar(memory[memory_pointer]);
        }
        //write to memory the input from user.
        else if (code[code_pointer] == ',') {
            memory[memory_pointer] = getchar();
        }
        //if false, pass this loop.
        else if (code[code_pointer] == '[') {
            if (memory[memory_pointer] == 0) {
                code_pointer++;
                while (loop_counter > 0 || code[code_pointer] != ']') {
                    if (code[code_pointer] == '[') { ++loop_counter; }
                    if (code[code_pointer] == ']') { --loop_counter; }
                    code_pointer++;
                }
            }
        }
        //if true, go back to start of loop.
        else if (code[code_pointer] == ']') {
            code_pointer--;
            while (loop_counter > 0 || code[code_pointer] != '[') {
                if (code[code_pointer] == ']') { ++loop_counter; }
                if (code[code_pointer] == '[') { --loop_counter; }
                code_pointer--;
            }
            code_pointer--;
        }
    }
    free(code);
    return 0;
}

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ structs -----------------------------
typedef struct rectangle {
	int weight;
	int length;
} Rectangle;

typedef struct node
{
    int data;
    struct Node* next;
} Node;

//prints items in list
void print_list(Node* head)
{
    if (head == NULL)
    {
        printf("list is empty");
        return;
    }
    do
    {
        printf("%d->", head->data);
        head = head->next;
    } while (head != NULL);

    printf("NULL\n");
}

//frees items in list.
void free_list(Node** head)
{
    Node* current = *head;
    Node* next;

    while (current != NULL)
    {
        next = current->next;
        free(current);
        current = next;
    }
    *head = NULL;
}

Node* create_node(int data)
{
    Node* new_node = (Node*)malloc(sizeof(Node));
    new_node->data = data;
    new_node->next = NULL;
    return new_node;
}

//function adds new node containing new_data at end of list.
void append(Node** head, int new_data)
{
    Node* last = *head;
    Node* new_node = (Node*)malloc(sizeof(Node));
    new_node->data = new_data;
    new_node->next = NULL;
    if (*head == NULL)
    {
        *head = new_node;
        return;
    }
    while (last->next != NULL)
    {
        last = last->next;
    }
    last->next = new_node;
}


// ------------------------------ functions -----------------------------

void rtos(Node** dims, int weight, int length) {

	if (weight == length) {
        append(dims, weight);
		printf("%d\n", weight);
	}
	else if (weight < length) {
        append(dims, weight);
		printf("%d, ", weight);
		rtos(dims, weight, length - weight);
	}
	else {
        append(dims, length);
		printf("%d, ", length);
		rtos(dims, weight - length, length);
	}
}


int main(int argc, char* argv[]) {
	Node* dimentions = NULL;
	Rectangle rec1 = { 5, 3 };
	rtos(&dimentions, rec1.weight, rec1.length);
    print_list(dimentions);
    free(dimentions);
}
// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ functions -----------------------------
void hanoi_rec(int disks, char from, char helper, char to) {
	if (disks == 1) {
		printf("Move disk 1 from %c to %c\n", from, to);
		return;
	}
	hanoi_rec(disks - 1, from, to, helper);
	printf("Move disk %d from %c to %c\n", disks, from, to);
	hanoi_rec(disks - 1, helper, from, to);
	return;
}


void hanoi(int disks) {
	hanoi_rec(disks, 'A', 'B', 'C');
}


int main(int argc, char* argv[])
{
	hanoi(3);
}
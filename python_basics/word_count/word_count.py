import sys


def word_cleanup(word):
    while word != "" and (not word[0].isalpha() or not word[-1].isalpha()):
        if word != "" and not word[0].isalpha():
            word = word[1:]
        if word != "" and not word[-1].isalpha():
            word = word[:-1]
    return word


def get_word_dict(filename):
    words = {}

#   insert new word to dictionary
    def insert_word(wrd):
        if wrd in words.keys():
            words[wrd] += 1
        elif wrd != "":
            words[wrd] = 1

    with open(filename, 'r') as txt:
        for line in txt:
            for word in line.split():
                word = word.lower()
                word = word_cleanup(word)

                if word.find("--") != -1:
                    for sub_word in word.split("--"):
                        sub_word = word_cleanup(sub_word)
                        insert_word(sub_word)
                else:
                    insert_word(word)

    return words


def print_words(filename):
    words = get_word_dict(filename)
    words = {k: v for k, v in sorted(words.items(), key=lambda item: item[0])}
    for word in words.keys():
        print(word + " " + str(words[word]))


def print_top(filename):
    words = get_word_dict(filename)
    words = {k: v for k, v in sorted(words.items(), key=lambda item: -item[1])}
    counter = 0
    for word in words:
        if counter > 20:
            break
        else:
            print(word + " " + str(words[word]))
            counter += 1


def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | ---topcount} file")
        sys.exit(1)

    option  = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()

"""
Black Biss - Basic Python

Write a function that replace any second word with the word "Corona".
"""


def word_pandemic(in_array):
    word_list = in_array.split()
    i = 0
    result = ""
    while i < word_list.__len__():
        if i % 2 == 1:
            result += "Corona"
        else:
            result += word_list[i]
        if i < word_list.__len__() - 1:
            result += ' '
        i += 1
    return result


# small test to check it's work.
if __name__ == '__main__':
    base_word = "Koala Bears are the cutest"
    sick_word = "Koala Corona are Corona cutest"

    if word_pandemic(base_word) == sick_word:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")

def camel_case(sentence):
    result = ""
    for char in sentence:
        if char.isupper() and result != "":
            result += ' '
        result += char

    return result


# small test to check it's work.
if __name__ == '__main__':
    sent_base = "camelCaseIsAwesome"
    sent_expected = "camel Case Is Awesome"

    if camel_case(sent_base) == sent_expected:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
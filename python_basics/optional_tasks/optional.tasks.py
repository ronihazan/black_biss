def task_1(array_1, array_2):
    if array_1.__len__() != array_2.__len__():
        return None
    return [(array_1[i], array_2[i]) for i in range(array_1.__len__())]


def task_2(array):
    return {array[i][0]: array[i][1] for i in range(array.__len__())}


def task_3_1(array_1, array_2):
    if not array_1:
        return []
    return [(array_1[0], array_2[0])] + task_3_1(array_1[1:], array_2[1:])


def task_3_2(array):
    if not array:
        return {}
    tpl = array.pop(0)
    return {**{tpl[0]: tpl[1]}, **task_3_2(array)}


def task_4(array1, array2):
    if not array1:
        return {}
    return {**{array1.pop(0): array2.pop(0)}, **task_4(array1, array2)}


def task_5(array_long, array_short):
    return task_4(array_long[:array_short.__len__()], array_short)

"""
Black Biss - Advance Python

Write a function that calculate how many month one should save money to buy new car.
"""


def home_economics(startPriceOld, startPriceNew, savingPerMonth, percentLossByMonth):
    result = 0
    savings = 0
    money = startPriceOld
    updatedPriceOld = startPriceOld
    updatedPriceNew = startPriceNew
    updatedPercentLoss = percentLossByMonth
    while money < updatedPriceNew:
        result += 1
        if result % 2 == 0 and result != 0:
            updatedPercentLoss += 0.5
        updatedPriceOld *= (100 - updatedPercentLoss) / 100
        updatedPriceNew *= (100 - updatedPercentLoss) / 100
        savings += savingPerMonth
        money = updatedPriceOld + savings

    return result, round(money - updatedPriceNew)


# small test to check it's work.
if __name__ == '__main__':

    ret = home_economics(2000, 8000, 1000, 1.5)
    if ret[0] == 6 and ret[1] == 766:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")

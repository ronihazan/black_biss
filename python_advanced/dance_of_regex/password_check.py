"""
Black Biss - Advance Python

Write a function that read one line from the user with many potentional passwords seperated by comma ","
and print out only the valid password - by the following rules:
    * contain letter from each of the characters-sets:
        lower-case english, upper-case english, decimal digit,
        special characters *&@#$%^
    * dosn't contain other symbols (ignore white-spaces)
    * length between 6 and 12

"""
import sys


def validate_passwords():
    valid_passwords = []
    passwords = input().split(", ")
    specialChars = ['*', '&', '^', '%', '$', '#', '@']
    for password in passwords:
        lowerChar = False
        number = False
        upperChar = False
        specialChar = False
        properLength = False
        if 6 <= password.__len__() <= 12:
            properLength = True
        for char in password:
            if char.islower():
                lowerChar = True
            if char.isupper():
                upperChar = True
            if char.isdigit():
                number = True
            if char in specialChars:
                specialChar = True
        if lowerChar and number and upperChar and specialChar and properLength:
            valid_passwords.append(password)
    for password in valid_passwords:
        if password != valid_passwords[valid_passwords.__len__()-1]:
            sys.stdout.write(password + ", ")
        else:
            sys.stdout.write(password)


# small test to check it's work.
if __name__ == '__main__':
    validate_passwords()

"""
Black Biss - Advance Python

Write a function that suggest which word is the Pirates meant to write.
"""


def dejumble(word, potentional_words):
    result = []
    chars_in_word = [c for c in word]
    for potentional_word in potentional_words:
        valid_chars = [c for c in potentional_word]
        if set(valid_chars) == set(chars_in_word):
            result.append(potentional_word)
    return result


# small test to check it's work.
if __name__ == '__main__':

    ret = dejumble("orspt", ["sport", "parrot", "ports", "matey"])
    if len(ret) == 2 and set(ret) == set(["sport", "ports"]):
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")

"""
Black Biss - Advance Python

Write a function that get a path to file that encrypted with 3 english-small-letter xor key, and print the
deciphered text.
"""


#   checks the option makes sense in this particular match, I removed chars which seemed unlikely to be in the code
def valid_code_option(option, coded_char):
    if 0 <= option ^ coded_char <= 126 and chr(option ^ coded_char) not in ['#', '{', '}']:
        return True
    return False


def crypto_breaker(file_path):
    uncoded_message = ""
    counter = 0
    options = [[chr(i) for i in range(97, 123)] for j in range(3)]
    with open(file_path, 'r') as txt:
        for line in txt:
            try:
                coded_chars = [int(char) for char in line.split(',')]
            except ValueError:
                print("file values invalid")
                break
            for i in range(coded_chars.__len__()):
                counter += 1
                if True in (option.__len__() > 1 for option in options):
                    options[i % 3][:] = (x for x in options[i % 3] if valid_code_option(ord(x), coded_chars[i]))
                else:
                    break
            if False in (option.__len__() == 1 for option in options):
                print("code unclear")
            else:
                for i in range(coded_chars.__len__()):
                    coded_chars[i] ^= ord(options[i % 3][0])
                    uncoded_message += chr(coded_chars[i])
                print(uncoded_message)


# small test to check it's work.
if __name__ == '__main__':
    crypto_breaker('big_secret.txt')

import math
"""
Black Biss - Advance Python

Write a function that calculate how far is our ejected pilot.
Run this program and insert the following lines (each end with Enter):

UP 5
DOWN 3
LEFT 3
RIGHT 2
0

and the result should be 2
"""


def ejected():
    coordinates = (0, 0)
    trans = {"UP": (0, 1), "DOWN": (0, -1),
             "LEFT": (-1, 0), "RIGHT": (1, 0)}
    action = input()
    while action != "0":
        action = action.split()
        coordinates = tuple([int(action[1]) * trans[action[0]][i] + coordinates[i] for i in range(2)])
        action = input()
    return round(math.sqrt(coordinates[0]**2 + coordinates[1]**2))


# small test to check it's work.
if __name__ == '__main__':
    print(ejected())

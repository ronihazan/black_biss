"""
Black Biss - Advance Python

Write a function that get array of string, each represent a row in 2-dim map, when:
    'X' is start / destination point
    '-' is road right-left
    '|' is road up-down
    '+' is a turn in the road, can be any turn (which isn't continue straight)
    path = ["           ",
            "X-----+    "
            "      |    "
            "      +---X",
            "           "]

    # Note: this grid is only valid when starting on the right-hand X, but still considered valid
    path = ["                      ",
            "   +-------+          ",
            "   |      +++---+     ",
            "X--+      +-+   X     "]
    nevigate_validate(path)  # ---> True
"""

''' the function is called when entering an intersection,
    it decides which way the delivery person should turn based on his current motion. 
    if something is wrong and the turn is illegal the function returns False'''


#   the function looks both ways, and if there's a clear way to go it takes a step that way
def look_both_ways(rows, cur_coordinates, current_motion, floating_chars, were_we_here):
    change_dir = {'-': '|', '|': '-'}
    directions = {'-': [1, 0], '|': [0, 1]}
    look_sides = list()
    look_sides.append([cur_coordinates[0] - current_motion[1] * directions[current_motion[0]][0],
                       cur_coordinates[1] + current_motion[1] * directions[current_motion[0]][1]])
    look_sides.append([cur_coordinates[0] + current_motion[1] * directions[current_motion[0]][0],
                       cur_coordinates[1] - current_motion[1] * directions[current_motion[0]][1]])
    exists_sides = [side for side in look_sides if 0 <= side[0] < rows.__len__() and 0 <= side[1] < len(rows[0])]
#   if it's unclear which way we should go return False
    if all(rows[side[0]][side[1]] in [' ', current_motion[0]] for side in exists_sides):
        return False
    if exists_sides.__len__() > 1:
        if all(rows[side[0]][side[1]] in ['+', 'X', change_dir[current_motion[0]]] for side in exists_sides):
            return False
#   we know where we should go! let's change our motion and take a step
    for side in exists_sides:
        if rows[side[0]][side[1]] in ['+', 'X', change_dir[current_motion[0]]]:
            current_motion[1] = side[0] - cur_coordinates[0] + side[1] - cur_coordinates[1]
            current_motion[0] = change_dir[current_motion[0]]
            break
    if not take_step(rows, cur_coordinates, current_motion, floating_chars, were_we_here):
        return False
    return True


#   this function takes a step for our delivery man, checks the step was legal
def take_step(rows, cur_coordinates, current_motion, floating_chars, were_we_here):
    if current_motion[0] == '-':
        cur_coordinates[1] += current_motion[1]
    else:
        cur_coordinates[0] += current_motion[1]
    if not 0 <= cur_coordinates[0] < rows.__len__() or not 0 <= cur_coordinates[1] < rows[0].__len__():
        return False
    if were_we_here[cur_coordinates[0]][cur_coordinates[1]]:
        return False
    if rows[cur_coordinates[0]][cur_coordinates[1]] not in ['+', 'X', current_motion[0]]:
        return False
    update_arrays(cur_coordinates, floating_chars, were_we_here)
    return True


#   this function is called when we're starting to check our path, and takes the first step
def where_to_start(rows, cur_coordinates, current_motion):
    sides = []
    for i in range(-1, 2, 2):
        if 0 <= cur_coordinates[1]+i < len(rows[0]):
            if rows[cur_coordinates[0]][cur_coordinates[1]+i] in ['X', '-', '+']:
                sides.append([cur_coordinates[0], cur_coordinates[1]+i])
        if 0 <= cur_coordinates[0]+i < rows.__len__():
            if rows[cur_coordinates[0]+i][cur_coordinates[1]] in ['X', '|', '+']:
                sides.append([cur_coordinates[0]+i, cur_coordinates[1]])
    if sides.__len__() != 1:
        return False
    our_way = sides[0]
    if our_way[0] - cur_coordinates[0] == 0:
        current_motion[0] = '-'
        current_motion[1] = our_way[1] - cur_coordinates[1]
    else:
        current_motion[0] = '|'
        current_motion[1] = our_way[0] - cur_coordinates[0]
    return our_way


def update_arrays(cur_coordinates, floating_chars, were_we_here):
    floating_chars[cur_coordinates[0]][cur_coordinates[1]] = False
    were_we_here[cur_coordinates[0]][cur_coordinates[1]] = True
    return


def one_way_valid(rows, cur_coordinates):
    floating_chars = [[char != ' ' for char in row] for row in rows]
    were_we_here = [[False for char in row] for row in rows]
    update_arrays(cur_coordinates, floating_chars, were_we_here)
#   current_motion is a tuple, first element is '|' for up/down and '-'...
#    second element is -1 for up/left and 1...
    current_motion = [None, None]

#   we're starting from X, we need to figure out where we're heading.
    cur_coordinates = where_to_start(rows, cur_coordinates, current_motion)
    if not cur_coordinates:
        return False
    update_arrays(cur_coordinates, floating_chars, were_we_here)

#   let's find our destination
    while rows[cur_coordinates[0]][cur_coordinates[1]] != 'X':
        while rows[cur_coordinates[0]][cur_coordinates[1]] != '+':
            if rows[cur_coordinates[0]][cur_coordinates[1]] == 'X':
                break
            if not take_step(rows, cur_coordinates, current_motion, floating_chars, were_we_here):
                return False
#   look which way you should turn and take step
        if rows[cur_coordinates[0]][cur_coordinates[1]] == 'X':
            break
        if not look_both_ways(rows, cur_coordinates, current_motion, floating_chars, were_we_here):
            return False
#   check if there are any floating chars in our map
    for row in floating_chars:
        for char in row:
            if char:
                return False
    return True


def path_to_array(path):
    lngt = len(path[0])
    array_path = []
    for item in path:
        array_path += [char for char in [item[i:i + lngt] for i in range(0, len(item), lngt)]]
    return array_path


def nevigate_validate(path):
    proper_chars = ['-', '|', ' ', '+', 'X']
    for strg in path:
        if len(strg) % len(path[0]) != 0:
            return False
#   check the lines received in the input are full and the same length
    rows = path_to_array(path)
#   I turn the input into a two dimension array so it's easier to work with
    x_coords = []
    for i in range(0, rows.__len__()):
        for j in range(0, rows[i].__len__()):
            if rows[i][j] not in proper_chars:
                return False
            if rows[i][j] == 'X':
                x_coords.append([i, j])
    if x_coords.__len__() != 2:
        return False
    return one_way_valid(rows, x_coords[0]) or one_way_valid(rows, x_coords[1])


# small test to check it's work.
if __name__ == '__main__':
    path = ["           ",
            "X-----+    "
            "      |    "
            "      +---X",
            "           "]
    valid = nevigate_validate(path)

    path = ["           ",
            "X--|--+    "
            "      -    "
            "      +---X",
            "           "]
    invalid = nevigate_validate(path)
    if valid and not invalid:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")

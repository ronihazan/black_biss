/**
 * @file substrings.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that check if one string is substring of the other and return pointer
 * to the start of the substring in the string.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ functions -----------------------------
char* sub_string(const char* a, const char* b)
{
	for (int i = 0; i < strlen(a); ++i)
	{
		int counter = 0;
		int j = i;
		while(a[j] == b[counter])
		{
			++counter;
			++j;
		}
		if (counter == strlen(b))
		{
			return b;
		}
	}
	return NULL;
}

char* my_strstr(const char* a, const char* b)
{
	if (strlen(a) >= strlen(b))
	{
		return sub_string(a, b);
	}
	else
	{
		return sub_string(b, a);
	}
}


int main(int argc, char * argv[])
{
   char s1[] = "AAAAAAACCCAAAAAAAC";
   char s2[] = "CCC";

   char * ret = my_strstr(s1, s2);
   printf("The substring is: %s\n", ret);

	return 0;
}
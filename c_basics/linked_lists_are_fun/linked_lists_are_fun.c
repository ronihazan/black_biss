// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
	
// ------------------------------ functions -----------------------------
typedef struct node
{
    int data;
    struct Node* next;
} Node;

//prints items in list
void print_list(Node* head)
{
    if (head == NULL)
    {
        printf("list is empty");
        return;
    }
    do
    {
        printf("%d->", head->data);
        head = head->next;
    } while (head != NULL);

    printf("NULL\n");
}

//frees items in list.
void free_list(Node** head)
{
    Node* current = *head;
    Node* next;

    while (current != NULL)
    {
        next = current->next;
        free(current);
        current = next;
    }
    *head = NULL;
}

Node* create_node(int data)
{
    Node* new_node = (Node*)malloc(sizeof(Node));
    new_node->data = data;
    new_node->next = NULL;
    return new_node;
}

//function adds new node containing new_data at end of list.
void append(Node** head, int new_data)
{
    Node* last = *head;
    Node* new_node = (Node*)malloc(sizeof(Node));
    new_node->data = new_data;
    new_node->next = NULL;
    if (*head == NULL)
    {
        *head = new_node;
        return;
    }
    while (last->next != NULL)
    {
        last = last->next;
    }
    last->next = new_node;
}

//if N is between 0 and list's length, the function inserts new node in place N with new_data.
void insert(Node **head, int new_data, size_t N)
{
    Node* new_node = (Node*)malloc(sizeof(Node));
    Node* cur_node = *head;
    int cur_place = 0;
    new_node->data = new_data;
    if (N == 0)
    {
        new_node->next = *head;
        *head = new_node;
    }
    
    while (cur_node->next != NULL && cur_place < N - 1) 
    {
        cur_node = cur_node->next;
        ++cur_place;
    }
    if (cur_place < N - 1)
    {
        return;
    }
    new_node->next = cur_node->next;
    cur_node->next = new_node;
}

//small check to see it works.
int main(int argc, char* argv[])
{
    Node* new_node = create_node(5);
    append(&new_node, 3);
    print_list(new_node);
    insert(&new_node, 8, 2);
    insert(&new_node, 9, 2);
    print_list(new_node);
    free_list(&new_node);

    return 0;
}
#define _CRT_SECURE_NO_WARNINGS
/**
 * @file word_count.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that print how many words are in a given file.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 * @section DESCRIPTION
 * The system get file path as argument. Open it and count the word's in it.
 * Input  : file_path
 * Output : print out the amount of words.
 */
 

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h> 

// ------------------------------ defines ------------------------------
#define UNABLE_TO_OPEN_FILE (-1)
#define UNABLE_TO_CLOSE_FILE (-2)

// ------------------------------ functions -----------------------------
int main(int argc, char * argv[])
{
    size_t word_count = 0;
    FILE* file;
    char chr;
    bool inside_word = false;

    if (argc < 2)
    {
        printf("missing file_path parameter.\n");
        return EXIT_FAILURE;
    }

    file = fopen(argv[1], "r");
    if (file == NULL)
    {
        printf("Failed to open file %s\n", argv[1]);
        return UNABLE_TO_OPEN_FILE;
    }

    do
    {
        chr = fgetc(file);
        if ((chr >= 'a' && chr <= 'z') || (chr >= 'A' && chr <= 'Z')) 
        {
            if (inside_word == false)
            {
                ++word_count;
            }
            inside_word = true;
        }
        else
        {
            //following lines are possible to add so words like "won't" count as one word.
            /* if (inside_word == TRUE && chr == '\'') {
                continue;
            } */
            inside_word = false;
        }
    } while (chr != EOF);

    printf("The file %s contain %u words.\n", argv[1], word_count);
    if (0 != fclose(file))
    {
        printf("Failed to close file %s\n", argv[1]);
            return UNABLE_TO_CLOSE_FILE;
    }

	return 0;
}
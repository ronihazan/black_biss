// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ functions -----------------------------
void print_codes(char* codes, int code_size, int codes_num) {
	for (int i = 0; i < codes_num; ++i) {
		printf("%s ", codes + i * code_size);
	}
}

int how_many_codes(char* guess) {
	int options[10] = { 1, 2, 3, 2, 3, 4, 3, 2, 4, 2 };
	int op_codes = 1; // num of optional codes
	for (int i = 0; i < strlen(guess); ++i) {
		op_codes *= options[ctoi(guess[i])];
	}
	return op_codes;
}

void add_to_counters(int* counters, char* guess, int options[], int code_len) {
	int one_aside = 1;
	for (int i = code_len - 1; i > -1; --i) {
		if (one_aside == 1) {
			if (counters[i] + 1 < options[ctoi(guess[i])] || i == 0) {
				counters[i] += 1;
				one_aside = 0;
			}
			else {
				counters[i] = 0;
			}
		}
		else {
			break;
		}
	}
}

void code_add(char* codes, int* codes_added, int code_len, char new_code[]) {
	for (int i = 0; i < code_len; ++i) {
		*(codes + (*codes_added) * (code_len + 1) + i) = *(new_code + i);
	}
	++(*codes_added);
}

int ctoi(char c) {
	return (int)(c - '0');
}

char** possible_codes(char* guess) {
	int code_len = strlen(guess);
	int options[10] = { 1, 2, 3, 2, 3, 4, 3, 2, 4, 2 };
	char dict[10][4] = { {'8'},  {'2', '4'}, {'1', '3', '5'}, {'2', '6'}, {'1', '5', '7'},
		{'2', '4', '6', '8'}, {'3', '5', '9'}, {'4', '8'}, {'5', '7', '9', '0'}, {'6', '8'} };
	//count how many codes are we expecting to return and declare space for them.
	int codes_num = how_many_codes(guess);
	char* codes = (char*)calloc(codes_num * (code_len + 1), sizeof(char));
	//define counters in order to follow our code building.
	int* counters = (int*)calloc(strlen(guess), sizeof(int));
	for (int i = 0; i < strlen(guess); i++) {
		counters[i] = 0;
	}
	//build each code using our counters, and add it to codes.
	int codes_added = 0;
	char* new_code = (char*)malloc(strlen(guess)+1);
	*(new_code + strlen(guess)) = '\0';
	while(counters[0] < options[ctoi(guess[0])]) {
		for (int i = 0; i < code_len; ++i) {
			new_code[i] = dict[ctoi(guess[i])][counters[i]];
		}
		code_add(codes, &codes_added, code_len, new_code);
		add_to_counters(counters, guess, options, code_len);
	}

	free(new_code);
	free(counters);
	counters = NULL;
	new_code = NULL;
	return codes;
}

int main(int argc, char* argv[]) {
	char* guess = "0108";
	char* some = possible_codes(guess);
	print_codes(some, strlen(guess) + 1, how_many_codes(guess));
	free(some);
	some = NULL;
	return 0;
}

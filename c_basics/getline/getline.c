/**
 * @file getline.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that read lines from FILE (file or stdin).
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ defines ------------------------------
#define ERROR (-1)
#define DEFAULT_SIZE (64)

// ------------------------------ functions -----------------------------
size_t my_getline(char ** lineptr, size_t * n, FILE * stream)
{
    char next_char;
    char* new_lineptr;
    size_t result = 0;

    //check if parameters are valid.
    if (lineptr == NULL)
    {
        return ERROR;
    }
    if (n == NULL)
    {
        return ERROR;
    }
    if (*lineptr == NULL)
    {
        *n = DEFAULT_SIZE;
        *lineptr = (char*)malloc(*n);
    }

    size_t needed_mem = 2;

    //start reading chars from stream and put in buffer, in case more place needed use realloc.
    do
    {
        next_char = fgetc(stream);
        if (next_char == EOF)
        {
            fseek(stream, 0, SEEK_SET);
            return ERROR;
        }
        ++needed_mem;

        if (needed_mem > SIZE_MAX)
        {
            return ERROR;
        }

        if (needed_mem > * n)
        {
            new_lineptr = (char*)realloc(*lineptr, needed_mem + 16);
            if (new_lineptr == NULL)
            {
                return ERROR;
            }
            *lineptr = new_lineptr;
            *n = needed_mem + 10;
        }

        (*lineptr)[result] = next_char;
        ++result;
        
    } while (next_char != '\n');

    (*lineptr)[result] = '\0';
    ++result;
    new_lineptr = (char*)realloc(*lineptr, result);
    if (new_lineptr == NULL)
    {
        return ERROR;
    }
    *n = result;

    return result;
}

int main()
{
    char * buffer;
    size_t bufsize = 4;
    size_t characters;

    buffer = (char *) malloc(bufsize * sizeof(char));
    if (buffer == NULL)
    {
        perror("Unable to allocate buffer");
        exit(1);
    }

    printf("Type something: ");
    characters = my_getline(&buffer, &bufsize, stdin);
    printf("%zu characters were read.\n", characters);
    printf("You typed: '%s'\n", buffer);
    free(buffer);

    return(0);
}